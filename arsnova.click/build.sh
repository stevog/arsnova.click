#! /bin/bash

#https://github.com/Differential/meteor-mobile-cookbook/blob/master/iOS/Building.md
#https://github.com/meteor/meteor/wiki/How-to-submit-your-Android-app-to-Play-Store

# Install Android SDK:
# Download Android SDK Tools for Linux https://developer.android.com/studio/index.html#downloads
# Extract it to: /root/Android_SDK

# Install OpenJDK8 (newer versions maybe work, too)
# Install Gradle
# Init the Gradle wrapper in the Android SDK:
# mkdir -p /root/Android_SDK/tools/templates
# cd /root/Android_SDK/tools/templates
# gradle wrapper

export ANDROID_HOME=/root/Android_SDK

#remove last build files
rm -r .meteor/local/cordova-build/
rm -r ../arsnova.click-build/

#build app
meteor build --allow-superuser ../arsnova.click-build/production --server=https://quiz.tarent.de:443

# Ignore errors, if they come from Android, or iOS. We only want the web page. See if the production artifact is build:
ls ../arsnova.click-build/production/arsnova.click.tar.gz

# Install Docker
# Install docker-compose
# Paste the Docker files in the release
